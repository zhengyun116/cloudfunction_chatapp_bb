const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);



function deleteInvitedSenderIs(userId){


    return admin.database().ref(`/invites`).orderByChild('sender_id').equalTo(userId).on("value", function (snapshot) {

        console.log("invites->query : " + snapshot.val());

        if (snapshot.exists() && snapshot.hasChildren()){

            const allCount = snapshot.numChildren();
            let Counter = 0;

            snapshot.forEach(function (childSnapshot) {
                console.log("remove invites ->" + childSnapshot.val());

                childSnapshot.ref.remove().then(result => {
                    Counter ++;
                    if (Counter === allCount){
                        console.log("scanned all invites:" + allCount);
                        return allCount;
                    }
                });
            })
        }else{
            console.log("deleteInvitedSenderIs -> snapshot null : " + userId );
            return null;
        }


    });


}


function deleteUrlFeed(userId){

    console.log("deleteUrlFeed -> " + userId);

    return admin.database().ref("/url_feed").orderByChild('user_id').equalTo(userId).on("value", function (snapshot) {

        console.log("url_feed->query : " + snapshot.val());

        if (snapshot.exists() && snapshot.hasChildren()){

            let counter = 0;
            const allCount = snapshot.numChildren();
            snapshot.forEach(function (childSnapshot) {
                counter ++;
                console.log("remove url_feed ->" + childSnapshot.val());

                childSnapshot.ref.remove(error => {
                }).then(result => {

                    counter ++;
                    if (counter === allCount){
                        Console.log("scanned all urlfeed: " + allCount);
                        return allCount;
                    }

                });


            })
        }else{
            console.log("deleteUrlFeed -> snapshot null : " + userId );
            return null;
        }


    });



}


function deleteConversations(userId){

    console.log("deleteConversations -> " + userId);

    return admin.database().ref(`/conversations`).on("value", function (snapshot) {

        console.log("conversations->query");



        if (snapshot.exists() && snapshot.hasChildren()){

            const allCount = snapshot.numChildren();
            let counter = 0;
            snapshot.forEach(function (childSnapshot) {


                const chat = childSnapshot.val();
                console.log(chat);
                console.log("current chat msg -> " + counter + " - key : " + childSnapshot.key  );

                if (childSnapshot.exists() && childSnapshot.hasChildren()){
                    let childCounter = 0;
                    const  childAllCount = childSnapshot.numChildren();
                    childSnapshot.forEach(function (eachMsg) {
                        const msg = eachMsg.val();
                        childCounter ++;

                        const fromID = msg["fromID"];
                        if (fromID === userId){
                            eachMsg.ref.remove().then(result =>{

                                console.log("removed one msg :fromID: " + fromID);
                                if (childCounter === childAllCount){
                                    counter += 1;

                                    if (counter === allCount){
                                        console.log("scanned all conversations :" + allCount);
                                        return allCount;
                                    }
                                }

                            });
                        }else{

                            if (childCounter === childAllCount){
                                counter += 1;

                                if (counter === allCount){
                                    console.log("scanned all conversations :" + allCount);
                                    return allCount;
                                }
                            }
                        }


                    })

                }else{

                    console.log(" conversations childrens count null:");

                    counter += 1;

                    if (counter === allCount){
                        console.log("scanned all conversations :" + allCount);
                        return allCount;
                    }

                }

            })
        }else{
            console.log("deleteConversations -> snapshot null : " + userId );
            return null;
        }


    });



}


/***
 *
 * Deleted Tags from tags node in data store
 *
 ***/

function deleteTags(userId){

    console.log("deleteTags -> " + userId );

    return admin.database().ref(`/tags`).on("value", function (snapshot) {

        console.log("deleteTags->value");

        if (snapshot.exists() && snapshot.hasChildren()){
            let counter = 0;
            const allCount = snapshot.numChildren();
            snapshot.forEach(function (childSnapshot) {

                const creatorId = childSnapshot.val()['creator'];
                console.log("remove each user -> " + counter + " - key : " + childSnapshot.key + " - creatorId : " + creatorId + " -> tag_string : " + childSnapshot.val()['tag_string']);

                if (creatorId == userId){
                    childSnapshot.ref.remove(error=>{
                        console.log("result of remove : " + (error == null));
                    }).then(result => {
                        counter ++;
                        if (counter === allCount){
                            console.log("Scanned all tags :" + allCount);
                            return allCount;
                        }
                    });
                }else{
                    counter ++;
                    if (counter === allCount){
                        console.log("Scanned all tags :" + allCount);
                        return allCount;
                    }
                }



            })
        }else{

            console.log("deleteTags -> snapshot null : " + userId );
            return null;
        }


    });



}


/***
 *
 * Deleted UrlFeedLiker from url_feed node in data store
 *
 ***/

function deleteLikedUrlFeed(userId){

    console.log("deleteLikedUrlFeed -> " + userId );

    return admin.database().ref(`/url_feed`).on("value", function (snapshot) {

        console.log("deleteLikedUrlFeed->value");

        if (snapshot.exists() && snapshot.hasChildren()){
            let counter = 0;
            const allCount = snapshot.numChildren();
            snapshot.forEach(function (childSnapshot) {

                const likedCount = childSnapshot.val()['liked'];

                console.log("remove each user -> " + counter + " - key : " + childSnapshot.key + " - liked : " + likedCount + " -> liker_list : " + childSnapshot.val()['liker_list']);
                const likerListCount = childSnapshot.child('liker_list').numChildren();
                console.log('likerListCount : ' + likerListCount);

                if (likerListCount > 0 ){

                    let  counterLiker = 0;
                    childSnapshot.child('liker_list').forEach(function(childLikerSnapshot){

                        counterLiker ++;
                        if (childLikerSnapshot.exists()){
                            let likerId = childLikerSnapshot.key;
                            console.log('each likerId : ' + likerId);
                            if(userId == likerId) {
                                childLikerSnapshot.ref.remove().then(result=> {

                                    let newLiked = likedCount - 1;
                                    childSnapshot.child('liked').ref.set(newLiked, function(error){

                                    }).then(result=>{
                                        if (counterLiker == likerListCount){
                                            return true;
                                        }
                                    });


                                });
                            }else{
                                if (counterLiker == likerListCount){
                                    return true;
                                }
                            }
                        }else{
                            if (counterLiker == likerListCount){
                                return true;
                            }

                        }

                    })
                }

            })
        }else{

            console.log("deleteTags -> snapshot null : " + userId );
            return null;
        }

    });



}

/***
 *
 * Deleted User from users node in data store
 *
 ***/

function deleteContacts(userId, contacts){

    console.log("deleteContacts -> " + userId );

    return admin.database().ref(`/users`).on("value", function (snapshot) {

        console.log("contacts->query");



        if (snapshot.exists() && snapshot.hasChildren()){
            let counter = 0;
            const allCount = snapshot.numChildren();
            return snapshot.forEach(function (childSnapshot) {

                console.log("remove each user -> " + counter + " - key : " + childSnapshot.key + " -> contacts : " + childSnapshot.val()['contacts']);

                childSnapshot.ref.child('contacts').child(userId).remove(error=>{
                    console.log("result of remove : " + (error == null));
                }).then(result => {
                    counter ++;
                    if (counter === allCount){
                        console.log("Scanned all contacts :" + allCount);
                        return allCount;
                    }
                });


            })
        }else{

            console.log("deleteContacts -> snapshot null : " + userId );
            return null;
        }


    });



}



exports.deletedUser = functions.database.ref('/users/{userId}').onDelete(event => {

    // if (event.data.previous.exists()) {
    //     console.log('this is changed case in deletedEvent');
    //     return;
    // }
    // Exit when the data is deleted.
    if (!event.data.exists()) {
        console.log('this is deleted case in DeletedUser');

    }

    let oldData = event.data.previous.val();

    //
    console.log('Deleted user data' + oldData);
    // console.log(oldData);

    const userId = event.params.userId;
    const phone = oldData['phone'];
    const contacts = oldData['contacts'];
    const conversations = oldData['conversations'];
    const feed_tags = oldData['feed_tags'];
    const group_contacts = oldData['group_contacts'];


    // console.log(`deleted user ID :`);
    // console.log(userId);
    // console.log(`deleted user phone :`);
    // console.log(phone);
    // console.log(`deleted user contacts :`);
    // console.log(contacts);
    // console.log(`deleted user conversations :`);
    // console.log(conversations);
    // console.log(`deleted user feed_tags :`);
    // console.log(feed_tags);
    // console.log(`deleted user group_contacts :`);
    // console.log(group_contacts);


    /***
     *
     * groups created by user are removed from local, and user authentication data also is removed from local
     * social node social ID : email data must be removed from local also.
     * Hence, data must be removed is
     *  conevrsations
     *  phones
     *  invites
     *  url_feed
     *
     ***/

    const delTagsResult = deleteTags(userId);

    const delContactsResult = deleteContacts(userId, contacts);

    const deleteInvitedSenderIsResult = deleteInvitedSenderIs(userId);

    const deleteUrlFeedResult = deleteUrlFeed(userId);

    const deleteConversationsResult = deleteConversations(userId);

    // remove phone created by this user
    const deleteLikedUrlFeedResult = deleteLikedUrlFeed(userId);

    return Promise.all([delTagsResult, deleteLikedUrlFeedResult, deleteConversationsResult, delContactsResult, deleteInvitedSenderIsResult, deleteUrlFeedResult]).then(allResult =>{
        admin.database().ref(`/phones/${phone}`).remove().then(result =>{

            console.log("deleted phone");
            console.log(result);


            return admin.database().ref(`/feed_tags`).orderByChild('creator').equalTo(userId).on("value", function (snapshot) {

                console.log("feed_tags->query : " + snapshot.val());

                if (snapshot.exists() && snapshot.hasChildren()){

                    const  count = snapshot.numChildren();
                    var counter = 0;
                    snapshot.forEach(function (childSnapshot) {

                        console.log("remove feedtag ->" + childSnapshot);
                        childSnapshot.ref.remove().then(result => {
                            counter ++;
                            if (counter === count) {
                                console.log("scanned all feed_tags : " + count);
                                return count;
                            }
                        });
                    })
                }else{
                    console.log("feed_tags null");
                    return null;
                }


            });




        });
    });




});

